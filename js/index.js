"use strict";

//  1) функции нужны что бы использовать одинаковый код в разных местах кода,
//     не переписывая его каждый раз.
//  2) аргументы это значения которые передаються в функцию с целью того, 
//     что бы функция далее работала с ними
//  3) оператор return возвращает то что в нем указанно и завершает выполнение функции
const add = (a,b) => a+b; 
const multiply = (a,b) => a*b; 
const devide = (a,b) => a/b; 
const minus = (a,b) => a-b; 
function calculate(){ 
    let firstNum = prompt('Enter your first number:'); 
    let secondNum = prompt('Enter your second number:'); 
    while(firstNum === '' || firstNum === null || isNaN(+firstNum) 
    || secondNum === '' || secondNum === null || isNaN(+secondNum)){
        let firstNumDeff = firstNum;
        let secondNumDeff = secondNum;
        firstNum = prompt('Enter your first number again:', firstNumDeff ); 
        secondNum = prompt('Enter your second number again:',secondNumDeff);
     }
    let mathOperation = prompt("Choose math operation('+' '-' '*' '/'):"); 
    while(mathOperation !== '+' && mathOperation !== '-' 
    && mathOperation !== '*' && mathOperation !== '/'
    || mathOperation === '' || mathOperation === null ){
        alert('Wrong operation selected!');
        mathOperation = prompt("Choose math operation('+' '-' '*' '/') again:");
    }
    switch(mathOperation){ 
        case '+': 
            return add(+firstNum, +secondNum); 
        case '-': 
            return minus(firstNum, secondNum); 
        case '*': 
            return multiply(firstNum, secondNum); 
        case '/': 
            if(+secondNum !== 0){
            return devide(firstNum, secondNum); 
            }else{
                alert('You can not on 0 devide!');
            }
 } 
} 
alert(calculate());